
example :: [a] -> Bool
example xs = case ()
             of _ | null xs       -> True
                _ | length xs > 5 -> True
                otherwise         -> False
